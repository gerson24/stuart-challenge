name := "stuart-challenge"

version := "0.1"

scalaVersion := "2.12.7"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-stream"         % "2.5.18",
  "com.typesafe.akka" %% "akka-stream-testkit" % "2.5.18" % Test,
  "org.scalatest"     %% "scalatest"           % "3.0.1" % Test,
  "ch.hsr"            % "geohash"              % "1.3.0"
)

scalafmtOnCompile := true
scalafmtVersion := "1.3.0"
