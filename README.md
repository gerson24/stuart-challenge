# Data Engineering Test

If you want to use a different file, you have to change `application.conf` with the new name of file.

Default file: test_points.txt


### Execution

The application takes 3 minutes to finish, this is for the unique prefix calculation (O(n2)):

> sbt run


### Test

> sbt test


### Design & Improvements

I have decided to implement it using akka streams.

The First flow process the input file and gets the points with its geohash.
The Second flow calculates unique prefix for each point with a recursive function.

Flows are very efficient in these cases because they offer a great parallelism but the recursive function needs a refactor because is very inefficient (n^2). 
Using some algorithm (calculate adjacents) or reducing the list of geohash in each iteration with tries it would be better.

Other interesting improvement could be use grafter or monad reader to dependencies injection.