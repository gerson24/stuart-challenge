package stuart.challenge.stream

import java.nio.file.Paths

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import org.scalatest.{Matchers, WordSpec}
import stuart.challenge.dao.Point

import scala.collection.immutable.Seq
import scala.concurrent.{Await, ExecutionContext}
import scala.concurrent.duration._

class PointStreamInSpec extends WordSpec with Matchers {

  "processFile" should {
    "get points with geoHash properly" in {
      val expected = Seq(
        Point(41.388828145321, 2.1689976634898, "sp3e3qe7mkcb"),
        Point(41.390743, 2.138067, "sp3e2wuys9dr"),
        Point(41.390853, 2.138177, "sp3e2wuzpnhr")
      )

      val source = Paths.get("src/main/resources/testing.txt")

      val result: Seq[Point] =
        Await.result(PointStreamInSpec.processFile(source), 10 seconds)

      result shouldBe expected
    }
  }

}

object PointStreamInSpec extends PointStreamIn {

  implicit val system: ActorSystem             = ActorSystem("test")
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  implicit val ec: ExecutionContext            = materializer.executionContext
}
