package stuart.challenge.utils

import org.scalatest.{Matchers, WordSpec}
import stuart.challenge.utils.Helpers.geoHash.uniquePrefix
import stuart.challenge.utils.Helpers.regex.pointFilter

class HelperSpec extends WordSpec with Matchers {

  "pointFilter" should {
    "return true when string contains two numbers" in {
      pointFilter("1,3") shouldBe true
      pointFilter("1.2,3") shouldBe true
      pointFilter("1.2,3.2") shouldBe true
      pointFilter("1,3.2") shouldBe true

    }

    "return false when string contains a char" in {
      pointFilter("1a,3.2") shouldBe false
      pointFilter("a") shouldBe false
    }

    "return false when string contains more than two numbers" in {
      pointFilter("1,2,3,4") shouldBe false
      pointFilter("1,3,2") shouldBe false
    }
  }

  "uniquePrefix" should {
    "obtain the prefix properly" in {
      val result1 = uniquePrefix(
        "sp3e3qe7mkcb",
        Set("sp3e3qe7mkcb", "sp3e2wuys9dr", "sp3e2wuzpnhr")
      )

      val result2 = uniquePrefix(
        "sp3e2wuys9dr",
        Set("sp3e3qe7mkcb", "sp3e2wuys9dr", "sp3e2wuzpnhr")
      )

      val result3 = uniquePrefix(
        "sp3e2wuzpnhr",
        Set("sp3e3qe7mkcb", "sp3e2wuys9dr", "sp3e2wuzpnhr")
      )

      val result4 = uniquePrefix(
        "foo",
        Set("fafasfasf", "asddsadsa", "dsadsadasd")
      )

      result1 shouldBe "sp3e3"
      result2 shouldBe "sp3e2wuy"
      result3 shouldBe "sp3e2wuz"
      result4 shouldBe "fo"
    }
  }
}
