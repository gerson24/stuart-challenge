package stuart.challenge.app

import java.nio.file.{Path, Paths}

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import com.typesafe.config.ConfigFactory
import stuart.challenge.stream.PointStreamIn
import stuart.challenge.utils.Constants.sourceConfig

import scala.concurrent.ExecutionContext

object PointApp extends App with PointStreamIn {

  implicit val system: ActorSystem             = ActorSystem("system")
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  implicit val ec: ExecutionContext            = materializer.executionContext

  val conf             = ConfigFactory.load
  val sourceFile: Path = Paths.get(conf.getString(sourceConfig))

  println("Calculating unique prefix...")

  for {
    points <- processFile(sourceFile)
    _ <- getUniquePrefixFromGeoHash(
      points,
      points
        .map(_.geoHash)
        .toSet // Use `Set` because geohashes are repeated
    )
  } yield {
    system.terminate()
  }
}
