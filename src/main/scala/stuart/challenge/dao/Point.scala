package stuart.challenge.dao

case class Point(
    latitude: Double,
    longitude: Double,
    geoHash: String
)
