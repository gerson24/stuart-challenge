package stuart.challenge.dao

case class PointWithPrefix(
    latitude: Double,
    longitude: Double,
    geoHash: String,
    uniquePrefix: String
) {
  override def toString: String =
    s"$latitude,$longitude,$geoHash,$uniquePrefix"
}
