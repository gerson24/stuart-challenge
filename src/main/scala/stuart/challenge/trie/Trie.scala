package stuart.challenge.trie

trait Trie extends Traversable[String] {

  def append(key: String)
  def findByPrefix(prefix: String): Set[String]
  def contains(word: String): Boolean
  def remove(word: String): Boolean
}
