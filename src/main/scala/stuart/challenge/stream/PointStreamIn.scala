package stuart.challenge.stream

import java.nio.file.Path

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{FileIO, Flow, Framing, Sink, Source}
import akka.util.ByteString
import akka.{Done, NotUsed}
import stuart.challenge.dao.{Point, PointWithPrefix}
import stuart.challenge.utils.Helpers.ShowPoint
import stuart.challenge.utils.Helpers.geoHash.{encode, uniquePrefix}
import stuart.challenge.utils.Helpers.regex.pointFilter

import scala.collection.immutable.{Seq, Set}
import scala.concurrent.{ExecutionContext, Future}
import stuart.challenge.trie.TrieNode
trait PointStreamIn {

  implicit val system: ActorSystem
  implicit val materializer: ActorMaterializer
  implicit val ec: ExecutionContext

  private val getLines: Flow[ByteString, ByteString, NotUsed] =
    Framing.delimiter(ByteString("\n"), maximumFrameLength = 128, allowTruncation = true)

  /**
    * Get all points from a file
    */
  def processFile(source: Path): Future[Seq[Point]] =
    FileIO
      .fromPath(source)
      .via(getLines)
      .map(_.utf8String)
      .filter(pointFilter)
      .map { elem =>
        val values = elem.split(",")
        val lat    = values(0).toDouble
        val lng    = values(1).toDouble

        Point(lat, lng, encode(lat, lng))
      }
      .runWith(Sink.seq)

  /**
    * Get unique prefix list from points
    */
  def getUniquePrefixFromGeoHash(
      points: Seq[Point],
      geoHashSet: Set[String]
  ): Future[Done] = {
    val trie = new TrieNode()
    geoHashSet.foreach(trie.append)

    Source(points)
      .mapAsyncUnordered(2000)( // Unordered is faster than ordered
        point =>
          Future(
            (point, uniquePrefix(point.geoHash, trie.findByPrefix(point.geoHash)))
        ))
      .map {
        case (point, prefix) =>
          PointWithPrefix(
            point.latitude,
            point.longitude,
            point.geoHash,
            prefix
          ).show()
      }
      .async
      .runWith(Sink.ignore)
  }
}
