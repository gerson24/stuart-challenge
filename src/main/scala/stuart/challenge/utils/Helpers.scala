package stuart.challenge.utils

import ch.hsr.geohash.GeoHash
import stuart.challenge.dao.PointWithPrefix

import scala.annotation.tailrec
import scala.collection.immutable.Set
import scala.util.matching.Regex

object Helpers {

  implicit class ShowPoint(p: PointWithPrefix) {
    def show(): Unit = println(p)
  }

  object geoHash {

    /**
      * Get Geohash with 12 characters of precision
      */
    def encode(latitude: Double, longitude: Double): String =
      GeoHash.geoHashStringWithCharacterPrecision(latitude, longitude, 12)

    /**
      * Get unique prefix from a geohash list
      */
    def uniquePrefix(geoHash: String, geoHashSet: Set[String]): String = {
      @tailrec
      def recUniPrefix(length: Int, n: Int): String = {
        if (length == n) geoHash
        else {
          val word = geoHash.substring(0, n)

          if (!geoHashSet.exists(str => str.contains(word) && str != geoHash)) word
          else recUniPrefix(length, n + 1)
        }
      }
      recUniPrefix(geoHash.length, 1)
    }
  }

  object regex {
    val pointPattern: Regex = "^[-+]?([0-9]+|[0-9]+\\.[0-9]+),[-+]?([0-9]+|[0-9]+\\.[0-9]+)$".r

    /**
      * String filter with `PointPattern`
      */
    def pointFilter(str: String): Boolean =
      pointPattern.findFirstMatchIn(str) match {
        case Some(_) => true
        case None    => false
      }
  }
}
